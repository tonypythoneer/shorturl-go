package validate

import (
	"github.com/go-playground/validator/v10"
)

var globalValidate *validator.Validate = validator.New()

func InitValidate() {
	globalValidate = validator.New()

	registerTagNameFunc(globalValidate)
	registerValidation(globalValidate)
}

func registerValidation(v *validator.Validate) {
	_ = v.RegisterValidation(isISO8601DateTag, isISO8601Date)
	_ = v.RegisterValidation(afterTag, isAfter)
}

func registerTagNameFunc(v *validator.Validate) {
	v.RegisterTagNameFunc(replacedTagNameWithJSON)
}
