package redis

import (
	"github.com/gofiber/storage/redis"
	"gitlab.com/tonypythoneer/shorturl-go/app/config"
)

var SimpleRedisStorage *redis.Storage

func InitSimpleRedisStorage() {
	SimpleRedisStorage = redis.New(redis.Config{
		Host:     config.Config.REDIS_HOST,
		Port:     config.Config.REDIS_PORT,
		Database: config.Config.REDIS_DATABASE,
		Reset:    false,
	})
}
