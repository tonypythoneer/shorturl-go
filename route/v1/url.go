package v1

import (
	"crypto/sha256"
	"time"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/tonypythoneer/shorturl-go/app/config"
	"gitlab.com/tonypythoneer/shorturl-go/service/shorturl"
	"gitlab.com/tonypythoneer/shorturl-go/util"
	"gitlab.com/tonypythoneer/shorturl-go/util/base62slug"
	"gitlab.com/tonypythoneer/shorturl-go/util/middleware"
)

// group

const (
	urlIDParam = "urlID"

	urlsPrefixRoute = "urls"
	singleUrlRoute  = ":" + urlIDParam
	pluralUrlRoute  = ""
)

func registerUrlGroup(parentGroup fiber.Router) fiber.Router {
	group := parentGroup.Group(urlsPrefixRoute)
	_ = group.Post(pluralUrlRoute,
		middleware.GenerateBodyParserMiddleware(new(ShortURLPostRequest)),
		middleware.ValidationMiddleware,
		findRegisteredShortURLController,
		createShortURLController)
	_ = group.Delete(singleUrlRoute, deleteShortURLController)
	return group
}

// POST

type ShortURLPostRequest struct {
	URL      string `json:"url" validate:"required,url"`
	ExpireAt string `json:"expire_at" validate:"required,ISO8601date,after=3s"`

	// temporary cache in instance
	hashedURL string
}

type ShortURLPostResponse struct {
	ID       string `json:"id"`
	ShortUrl string `json:"shortUrl"`
}

func (req *ShortURLPostRequest) GetHashedURL() string {
	if req.hashedURL != "" {
		return req.hashedURL
	}

	h := sha256.New()
	_, _ = h.Write([]byte(req.URL))
	hashedURL := string(h.Sum(nil))
	return hashedURL
}

func createShortURLController(c *fiber.Ctx) error {
	// retrieve clean req
	req, _ := middleware.GetRequestFromLocals(c).(*ShortURLPostRequest)

	// data process
	t, _ := util.PraseISO8601StringToTime(req.ExpireAt) // valid data, so ingore the error
	expiredPeriod := time.Until(t)

	// find unused random slug
	var slugIDString string
	for {
		slugIDBytes := base62slug.Encode()
		slugIDString = string(slugIDBytes)
		value, err := shorturl.GetURL(slugIDString)
		if err != nil {
			return err
		} else if value == nil { // inexistent
			break
		}
	}

	// save in redis
	if err := shorturl.CreateURL(shorturl.CreateURLOption{
		HashedKey:     req.GetHashedURL(),
		SlugKey:       slugIDString,
		URL:           req.URL,
		ExpiredPeriod: expiredPeriod,
	}); err != nil {
		return err
	}

	return c.Status(fiber.StatusCreated).JSON(fiber.Map{
		"id":       slugIDString,
		"shortUrl": "http://" + config.Config.GetHostAddress() + "/" + slugIDString,
	})
}

func findRegisteredShortURLController(c *fiber.Ctx) error {
	// retrieve clean req
	req, _ := middleware.GetRequestFromLocals(c).(*ShortURLPostRequest)

	// data process
	// exist hash key or not
	existedSlugIDBytes, err := shorturl.GetURL(req.GetHashedURL())
	if err != nil {
		return err
	} else if existedSlugIDBytes == nil {
		return c.Next()
	}

	existedSlugIDString := string(existedSlugIDBytes)
	return c.Status(fiber.StatusCreated).JSON(ShortURLPostResponse{
		ID:       existedSlugIDString,
		ShortUrl: "http://" + config.Config.GetHostAddress() + "/" + string(existedSlugIDString),
	})
}

// DELETE

func deleteShortURLController(c *fiber.Ctx) error {
	urlID := c.Params(urlIDParam, "")

	if err := shorturl.DeleteURL(urlID); err != nil {
		return err
	}
	return c.SendStatus(fiber.StatusNoContent)
}
