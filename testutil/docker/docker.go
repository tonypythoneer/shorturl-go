package docker

import (
	"log"

	"github.com/ory/dockertest/v3"
)

var dockerPool *dockertest.Pool

// because test files have to import this package to implement test cases,
// it, of course, triggers init function
func init() {
	pool, err := dockertest.NewPool("")
	if err != nil {
		log.Fatalf("Could not connect to docker service: %s", err)
	}
	log.Println("Success to connect to docker service")
	dockerPool = pool
}

type Container struct {
	//lint:ignore U1000 this private property only runs in private functions
	resource *dockertest.Resource
	//lint:ignore U1000 this private property only runs in private functions
	option ContainerOption
}

// first step: create container
func (c *Container) Create() func() func() {
	resource, err := dockerPool.Run(c.option.Repository, c.option.Tag, c.option.Env)
	if err != nil {
		log.Fatalln(err)
	}
	c.resource = resource
	return c.connect
}

// second step: connect container
func (c *Container) connect() func() {
	if err := dockerPool.Retry(c.option.ConnectionHandler(c.resource)); err != nil {
		log.Fatalf("could not connect to %s container: %s", c.resource.Container.Image, err)
	}
	return c.purge
}

// third step: purge container
func (c *Container) purge() {
	_ = dockerPool.Purge(c.resource) // this function for test, so handling error is unnecessary
}

// one-off function from
// (1) CreateContainer(opt) -> (2) connectContainer -> (3) purgeContainer
//
// example:
//     connectionRedisContainer := docker.CreateContainer(docker.RedisContainerOption)
//     removeRedisContainer := connectionRedisContainer()
//     removeRedisContainer()
func CreateContainer(opt ContainerOption) func() func() {
	c := Container{option: opt}
	return c.Create()
}
