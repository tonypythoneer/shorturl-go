package docker

import (
	"github.com/ory/dockertest/v3"
)

type ContainerOption struct {
	Repository        string
	Tag               string
	Env               []string
	ConnectionHandler func(resource *dockertest.Resource) func() error
}

var (
	RedisContainerOption = ContainerOption{
		"redis",
		"6.2.4-alpine",
		nil,
		simpleRedisStorageConnectionHandler}
)
