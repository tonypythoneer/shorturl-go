package middleware

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/tonypythoneer/shorturl-go/util/errorcode"
)

// extractor with locals

const (
	requestLocalKey = "request"
)

func GetRequestFromLocals(c *fiber.Ctx) interface{} {
	return c.Locals(requestLocalKey)
}

// middleware

func GenerateBodyParserMiddleware(request interface{}) func(*fiber.Ctx) error {
	return func(c *fiber.Ctx) error {
		if err := c.BodyParser(request); err != nil {
			return errorcode.NewError(errorcode.CodeInvalidJSON, err.Error())
		}
		c.Locals(requestLocalKey, request)
		return c.Next()
	}
}
