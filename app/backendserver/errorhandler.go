package backendserver

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/tonypythoneer/shorturl-go/util/errorcode"
)

func errorHandler(c *fiber.Ctx, err error) error {
	// custom
	if e, ok := err.(*errorcode.Error); ok {
		// Override status code if fiber.Error type
		return c.Status(e.GetStatusCode()).JSON(err)
	}

	// Default 500 statuscode
	code := fiber.StatusInternalServerError
	if e, ok := err.(*fiber.Error); ok {
		// Override status code if fiber.Error type
		code = e.Code
	}

	// Set Content-Type: text/plain; charset=utf-8
	c.Set(fiber.HeaderContentType, fiber.MIMETextPlainCharsetUTF8)

	// Return statuscode with error message
	return c.Status(code).SendString(err.Error())
}
