package v1

import (
	"github.com/gofiber/fiber/v2"
)

const (
	v1PrefixRoute = "v1"
)

func RegisterV1Group(parentGroup fiber.Router) fiber.Router {
	v1Group := parentGroup.Group(v1PrefixRoute)

	_ = registerUrlGroup(v1Group)
	return v1Group
}
