package main

import (
	"gitlab.com/tonypythoneer/shorturl-go/app/backendserver"
	"gitlab.com/tonypythoneer/shorturl-go/app/config"
	"gitlab.com/tonypythoneer/shorturl-go/app/redis"
	"gitlab.com/tonypythoneer/shorturl-go/app/validate"
)

func init() {
	// stateless initial
	config.InitConfig()
	validate.InitValidate()

	// stateful initial
	redis.InitSimpleRedisStorage()
}

func main() {
	backendserver.RunBackendServer()
}
