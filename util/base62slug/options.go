package base62slug

const (
	defaultLength int   = 6
	defaultSeed   int64 = 0
)

type Options struct {
	Length int
	Seed   int64
}

type SetOption func(*Options)

func WithLength(Length int) SetOption {
	return func(b *Options) {
		if Length > 0 {
			b.Length = Length
		}
	}
}

func WithSeed(seed int64) SetOption {
	return func(b *Options) {
		b.Seed = seed
	}
}

func applyOptions(setters ...SetOption) *Options {
	options := &Options{
		Length: defaultLength,
		Seed:   defaultSeed,
	}

	// Loop through each option
	for _, set := range setters {
		// Call the option giving the instantiated
		set(options)
	}

	// return the modified house instance
	return options
}
