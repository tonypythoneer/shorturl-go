package validate

import (
	"reflect"
	"time"

	"github.com/go-playground/validator/v10"
	"gitlab.com/tonypythoneer/shorturl-go/util"
)

const afterTag = "after"

func isAfter(fl validator.FieldLevel) bool {
	// expect time units are "ns", "us" (or "µs"), "ms", "s", "m", "h".
	duration, err := time.ParseDuration(fl.Param())
	if err != nil {
		return false
	}
	getOffsetNowFunc := makeOffsetNowFunc(duration)

	t, ok := extractTimeVariable(fl)
	if !ok {
		return false
	}
	return t.After(getOffsetNowFunc())
}

func extractTimeVariable(fl validator.FieldLevel) (time.Time, bool) {
	field := fl.Field()
	switch field.Kind() {
	case reflect.String:
		t, err := util.PraseISO8601StringToTime(field.String())
		if err != nil {
			return time.Time{}, false
		}
		return t, true
	default:
		return time.Time{}, false
	}
}

func makeOffsetNowFunc(duration time.Duration) func() time.Time {
	return func() time.Time {
		return time.Now().Add(duration)
	}
}
