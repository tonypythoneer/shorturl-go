package validate

import (
	"reflect"
	"strings"
)

func replacedTagNameWithJSON(fld reflect.StructField) string {
	jsonTag := fld.Tag.Get("json")
	name := strings.SplitN(jsonTag, ",", 2)[0]

	if name == "-" {
		return ""
	}
	return name
}
