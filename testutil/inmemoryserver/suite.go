package inmemoryserver

import (
	"github.com/gofiber/fiber/v2"
	"github.com/stretchr/testify/suite"
)

type InMemoryServerSuite struct {
	suite.Suite
	Server               *InMemoryServer
	RegisterRouteHandler func(router fiber.Router)
}

func (s *InMemoryServerSuite) SetupSuite() {
	s.Server = NewInMemoryServer(s.RegisterRouteHandler)
}
