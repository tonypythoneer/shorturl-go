package route

import (
	"github.com/gofiber/fiber/v2"
	v1 "gitlab.com/tonypythoneer/shorturl-go/route/v1"
)

const (
	apiPrefixRoute = "api"
)

func RegisterAPI(parentGroup fiber.Router) fiber.Router {
	apiGroup := parentGroup.Group(apiPrefixRoute)

	_ = v1.RegisterV1Group(apiGroup)
	return apiGroup
}
