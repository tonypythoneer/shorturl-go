package backendserver

import (
	"log"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"gitlab.com/tonypythoneer/shorturl-go/app/config"
	"gitlab.com/tonypythoneer/shorturl-go/route"
)

var HTTP_ADDRESS string

func RunBackendServer() {
	app := fiber.New(fiber.Config{
		ErrorHandler: errorHandler,
	})
	app.Use(recover.New())

	_ = route.RegisterAPI(app)
	_ = route.RegisterRedirect(app)

	if err := app.Listen(":" + config.Config.HTTP_PORT); err != nil {
		log.Fatalln(err.Error())
	}
}
