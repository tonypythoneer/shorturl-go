package config

import (
	"log"

	"github.com/kelseyhightower/envconfig"
)

var Config Configure

type Configure struct {
	// redis
	REDIS_HOST     string `default:"127.0.0.1"`
	REDIS_PORT     int    `default:"6379"`
	REDIS_DATABASE int    `default:"0"`

	// http
	HTTP_HOST string `default:"127.0.0.1"`
	HTTP_PORT string `default:"3000"`
}

func (c *Configure) GetHostAddress() string {
	return c.HTTP_HOST + ":" + c.HTTP_PORT
}

func InitConfig() {
	err := envconfig.Process("", &Config)
	if err != nil {
		log.Fatalln(err.Error())
	}
}
