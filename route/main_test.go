package route

import (
	"log"
	"os"
	"testing"

	"gitlab.com/tonypythoneer/shorturl-go/app/validate"
	"gitlab.com/tonypythoneer/shorturl-go/testutil/docker"
)

func TestMain(m *testing.M) {
	teardown := setup()
	code := m.Run()
	teardown()
	os.Exit(code)
}

func setup() func() {
	log.Printf("Trigger setup")
	validate.InitValidate()
	connectionRedisContainer := docker.CreateContainer(docker.RedisContainerOption)
	removeRedisContainer := connectionRedisContainer()

	var teardown = func() {
		log.Printf("Trigger teardown")
		removeRedisContainer()
	}
	return teardown
}
