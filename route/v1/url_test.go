package v1

import (
	"encoding/json"
	"fmt"
	"strconv"
	"testing"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"gitlab.com/tonypythoneer/shorturl-go/app/redis"
	"gitlab.com/tonypythoneer/shorturl-go/testutil/inmemoryserver"
	"gitlab.com/tonypythoneer/shorturl-go/util"
)

type ShortURLAPISuite struct {
	suite.Suite
	inmemoryserver.InMemoryServerSuite
}

func (s *ShortURLAPISuite) TestCreateShortURL() {
	// setup client service
	duration, _ := time.ParseDuration("4s")
	afterAFewMoments := time.Now().UTC().Add(duration)

	// send request
	statusCode, body, resErr := s.InMemoryServerSuite.Server.
		Post("http://example.com/" + urlsPrefixRoute).
		JSON(ShortURLPostRequest{
			URL:      "https://tw.search.yahoo.com/search?p=1&fr=yfp-search-sb",
			ExpireAt: afterAFewMoments.Format(util.ISO8601),
		}).Bytes()
	require.Nil(s.T(), resErr)
	require.Equal(s.T(), fiber.StatusCreated, statusCode)

	// extract body
	var res ShortURLPostResponse
	require.Nil(s.T(), json.Unmarshal(body, &res))

	// examine body
	slicedShortUrl := res.ShortUrl[len(res.ShortUrl)-len(res.ID):]
	require.Equal(s.T(), res.ID, slicedShortUrl)

	// check expired data after duration
	time.Sleep(duration + 100*time.Millisecond)
	value, redisErr := redis.SimpleRedisStorage.Get(res.ID)
	require.Nil(s.T(), redisErr)
	require.Nil(s.T(), value)
}

func (s *ShortURLAPISuite) TestCreShortURL() {
	// prepare data
	key := strconv.Itoa(time.Now().UTC().Second())
	require.Nil(s.T(), redis.SimpleRedisStorage.Set(key, []byte("bar"), 0))

	// send request
	statusCode, _, resErr := s.InMemoryServerSuite.Server.
		Delete(fmt.Sprintf("http://example.com/%s/%s", urlsPrefixRoute, key)).
		String()
	require.Nil(s.T(), resErr)
	require.Equal(s.T(), fiber.StatusNoContent, statusCode)

	// confirm deletion
	value, err := redis.SimpleRedisStorage.Get(key)
	require.Nil(s.T(), err)
	require.Nil(s.T(), value)
}

func TestShortURLAPISuite(t *testing.T) {
	s := new(ShortURLAPISuite)
	s.RegisterRouteHandler = func(router fiber.Router) {
		_ = registerUrlGroup(router)
	}
	suite.Run(t, s)
}
