package middleware

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/tonypythoneer/shorturl-go/app/validate"
	"gitlab.com/tonypythoneer/shorturl-go/util/errorcode"
)

func ValidationMiddleware(c *fiber.Ctx) error {
	if errors := validate.Validate(GetRequestFromLocals(c)); errors != nil {
		return errorcode.NewError(errorcode.CodeInvalidPayload, errors)
	}
	return c.Next()
}
