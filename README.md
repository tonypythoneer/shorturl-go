# short url

# 專案啟動方法

```shell
# 1. run infra # 請確保你的本機有 docker 和 docker-compose
make up
# 2. build/update vendor # 請用 golang 1.16
make mod
# 3. run service
make run
```

# 專案操作指令

```shell
# run infra
make up

# run service
make run

# build/update vendor
make mod

# run testing
make test
```

# 使用套件說明

* github.com/go-playground/validator/v10 v10.6.1
  * golang 驗證器，最常應用在 web application 接收 request 的驗證
* github.com/gofiber/fiber/v2 v2.11.0
  * golang web framework 的一種，單純看到它在效能比較文上他是目前 framework 中最好的，所以想嘗試他的開發體驗如何
* github.com/gofiber/storage/redis v0.0.0-20210602124502-f0233feaea00
  * gofiber 提供一種對 redis 的簡化操作介面，只需專注於在 key, value 操作即可
* github.com/kelseyhightower/envconfig v1.4.0
  * 環境變數初始化
* github.com/ory/dockertest/v3 v3.6.5
  * test 用的，可以臨時生成與 local 環境完全互不影響的 infra，讓 test case 可獨立運作
* github.com/stretchr/testify v1.7.0
  * 好用且常用的 test function 的函式庫
* github.com/valyala/fasthttp v1.26.0
  * fasthttp 的底層，有使用他的 test tool 來搭建 in-memory service

# project layout 說明

專案根目錄:
* app - 需要執行『初始化』操作的『服務』，或應用於專案的『全域變數』
* route - api handlers
* util - pure functions
* testutil - useful function for testing

## app 目錄

* validate - validator 的載入相關 function 和初始化的細節
* config - env 載入後的設定檔
* redis - 如其名
* backendserver - go fiber 的啟動方法

## route 目錄
* ./route - root layer endpoints
* ./route/v1 - v1 api endpoints

## util 目錄
* ./util - nothing
* ./util/middleware - gofiber middleware
* ./util/base62slug - short url generator
* ./util/errorcode - common error cases as error codes

## testutil 目錄
* ./testutil - nothing
* ./testutil/docker - 建立 testing 專屬的 infra

# 使用 fiber 提升撰寫 testcase 的舒適度

1. 原生 golang 普遍寫法

* 準備 raw data 和 native http request
   * 建立 request payload
   * 經過一番 io 操作後載入至 native http request
   * 填寫 header 的 content-type 資訊
* 用 native http request 餵食給 web application 並取得 http response
* response payload 需經過 io 操作，從 http response 取得實際資料載入

```go
func (s *URLEndpointTestSuite) TestTwo() {
	// service
	app := fiber.New()
	_ = registerUrlGroup(app.Group(""))

	// req
	jsonBytes, err := json.Marshal(ShortURLPostRequest{
		URL:      "https://tw.search.yahoo.com/search?p=1&fr=yfp-search-sb",
		ExpireAt: "2021-07-04T14:55:00Z",
	})
	require.Nil(s.T(), err)
	req := httptest.NewRequest(http.MethodPost, "/"+urlsPrefixRoute, bytes.NewBuffer(jsonBytes))
	req.Header.Add(fiber.HeaderContentType, fiber.MIMEApplicationJSON)
	resp, err := app.Test(req)

	require.Nil(s.T(), err)

	// extract body
	var respData ShortURLPostResponse
	defer resp.Body.Close()
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	require.Nil(s.T(), err)
	require.Nil(s.T(), json.Unmarshal(bodyBytes, &respData))

	slicedShortUrl := respData.ShortUrl[len(respData.ShortUrl)-len(respData.ID):]
	require.Equal(s.T(), respData.ID, slicedShortUrl)
}
```

2. 使用 fiber 推薦的寫法

優點:
* 使用 fasthttp 的 NewInmemoryListener 建立 in-memory listener 的 web application，作為內部測試用途
  * NewInmemoryListener 可讓 web application 不必監外部聽網路下啟動服務，且依然具備一般 web application 的行為
* fiber 有實作 client agent 來滿足常用測試情境
* 要讓 client agent 導向訪問 in-memory listener 做訪問，所以需要設置 agent.HostClient.Dial 這段

流程改善結果:
* 建立 web application with in-memory listener
* 使用 client agent 對該 web application 發送 request，且像大多數的 client request lib 的行為一致，且無需經過 io 行為操作
* agents.Bytes() 或 agents.String() 可立刻取得 status code, body, err，最重要的是 body 不是 io 類別的物件

結論:
少了繁複的 io 操作，專心在模擬情境上實作請求串接

```go
func (s *URLEndpointTestSuite) TestTwo() {
	// web service
	app := fiber.New(fiber.Config{DisableStartupMessage: true})
	_ = registerUrlGroup(app)

	ln := fasthttputil.NewInmemoryListener()
	go func() { require.Nil(s.T(), app.Listener(ln)) }()

	// setup req
	agent := fiber.Post("http://example.com/" + urlsPrefixRoute).
		JSON(ShortURLPostRequest{
			URL:      "https://tw.search.yahoo.com/search?p=1&fr=yfp-search-sb",
			ExpireAt: "2021-07-04T14:55:00Z",
		})
	agent.HostClient.Dial = func(addr string) (net.Conn, error) { return ln.Dial() }
    require.Nil(s.T(), agent.Parse())

	// send request
	statusCode, body, err := agent.Bytes()
	require.Nil(s.T(), err)
	require.Equal(s.T(), fiber.StatusCreated, statusCode)

	// extract body
	var res ShortURLPostResponse
	require.Nil(s.T(), json.Unmarshal(body, &res))

	// examine body
	slicedShortUrl := res.ShortUrl[len(res.ShortUrl)-len(res.ID):]
	require.Equal(s.T(), res.ID, slicedShortUrl)
}
```

# go unittest workflow (native and testify/suite version)

![golang_unittest_workflow](./doc/golang_unittest_workflow.jpg)
