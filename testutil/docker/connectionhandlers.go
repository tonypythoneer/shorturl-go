package docker

import (
	"strconv"

	"github.com/ory/dockertest/v3"
	"gitlab.com/tonypythoneer/shorturl-go/app/config"
	"gitlab.com/tonypythoneer/shorturl-go/app/redis"
)

const (
	defaultRedisPort = "6379/tcp"
)

func simpleRedisStorageConnectionHandler(resource *dockertest.Resource) func() error {
	return func() error {
		// config
		port, _ := strconv.Atoi(resource.GetPort(defaultRedisPort))
		config.Config.REDIS_PORT = port

		// connection
		redis.InitSimpleRedisStorage()

		// ping / test network
		return redis.SimpleRedisStorage.Delete("")
	}
}
