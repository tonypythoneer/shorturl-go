package util

import "time"

const (
	ISO8601   = "2006-01-02T15:04:05Z"
	ISO8601ms = "2006-01-02T15:04:05.000Z"
)

func PraseISO8601StringToTime(value string) (time.Time, error) {
	return time.Parse(ISO8601, value)
}
