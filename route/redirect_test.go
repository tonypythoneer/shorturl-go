package route

import (
	"strconv"
	"testing"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"gitlab.com/tonypythoneer/shorturl-go/app/redis"
	"gitlab.com/tonypythoneer/shorturl-go/testutil/inmemoryserver"
)

type ShortURLRedirectSuite struct {
	suite.Suite
	inmemoryserver.InMemoryServerSuite
}

func (s *ShortURLRedirectSuite) TestRedirectShortURLIfFound() {
	// prepare data
	key := strconv.FormatInt(time.Now().UTC().Unix(), 10)
	require.Nil(s.T(), redis.SimpleRedisStorage.Set(key, []byte("http://example.com/tmp/redirect"), 60*time.Second))

	// send request
	statusCode, body, resErr := s.InMemoryServerSuite.Server.
		Get("http://example.com/" + key).
		MaxRedirectsCount(1).
		String()
	require.Nil(s.T(), resErr, resErr)
	require.Equal(s.T(), fiber.StatusOK, statusCode)
	require.Equal(s.T(), "redirect", body)
}

func (s *ShortURLRedirectSuite) TestRedirectShortURLIfNotFound() {
	// send request
	statusCode, _, resErr := s.InMemoryServerSuite.Server.
		Get("http://example.com/notfound").String()
	require.Nil(s.T(), resErr)
	require.Equal(s.T(), fiber.StatusNotFound, statusCode)
}

func TestShortURLRedirectSuite(t *testing.T) {
	s := new(ShortURLRedirectSuite)
	s.RegisterRouteHandler = func(router fiber.Router) {
		_ = RegisterRedirect(router).
			Get("/tmp/redirect", func(c *fiber.Ctx) error {
				return c.SendString("redirect")
			})
	}
	suite.Run(t, s)
}
