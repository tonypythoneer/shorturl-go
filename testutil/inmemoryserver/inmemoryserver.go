package inmemoryserver

import (
	"log"
	"net"

	"github.com/gofiber/fiber/v2"
	"github.com/valyala/fasthttp/fasthttputil"
)

type InMemoryServer struct {
	ln *fasthttputil.InmemoryListener
}

func (s *InMemoryServer) Get(url string) *fiber.Agent {
	agent := fiber.Get(url)
	agent.HostClient.Dial = func(addr string) (net.Conn, error) { return s.ln.Dial() }
	return agent
}

func (s *InMemoryServer) Head(url string) *fiber.Agent {
	agent := fiber.Head(url)
	agent.HostClient.Dial = func(addr string) (net.Conn, error) { return s.ln.Dial() }
	return agent
}

func (s *InMemoryServer) Post(url string) *fiber.Agent {
	agent := fiber.Post(url)
	agent.HostClient.Dial = func(addr string) (net.Conn, error) { return s.ln.Dial() }
	return agent
}

func (s *InMemoryServer) Put(url string) *fiber.Agent {
	agent := fiber.Put(url)
	agent.HostClient.Dial = func(addr string) (net.Conn, error) { return s.ln.Dial() }
	return agent
}

func (s *InMemoryServer) Patch(url string) *fiber.Agent {
	agent := fiber.Patch(url)
	agent.HostClient.Dial = func(addr string) (net.Conn, error) { return s.ln.Dial() }
	return agent
}

func (s *InMemoryServer) Delete(url string) *fiber.Agent {
	agent := fiber.Delete(url)
	agent.HostClient.Dial = func(addr string) (net.Conn, error) { return s.ln.Dial() }
	return agent
}

func NewInMemoryServer(registerRouteHandler func(fiber.Router)) *InMemoryServer {
	app, ln := fiber.New(fiber.Config{DisableStartupMessage: true}), fasthttputil.NewInmemoryListener()
	if registerRouteHandler != nil {
		registerRouteHandler(app)
	}

	go func() {
		if err := app.Listener(ln); err != nil {
			log.Fatalf("Fail to run in memory server: %s", err)
		}
	}()
	return &InMemoryServer{ln: ln}
}
