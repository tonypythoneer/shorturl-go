package validate

import (
	"github.com/go-playground/validator/v10"
)

type ErrorResponse struct {
	InstanceField string
	JSONField     string
	Tag           string
	Param         string
	Value         interface{}
}

func Validate(variable interface{}) []*ErrorResponse {
	err := globalValidate.Struct(variable)
	if err == nil {
		return nil
	}

	validationErrors := err.(validator.ValidationErrors)
	errors := make([]*ErrorResponse, len(validationErrors))
	for index, err := range validationErrors {
		errors[index] = &ErrorResponse{
			InstanceField: err.StructNamespace(),
			JSONField:     err.Field(),
			Tag:           err.Tag(),
			Param:         err.Param(),
			Value:         err.Value(),
		}
	}
	return errors
}
