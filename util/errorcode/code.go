package errorcode

import "net/http"

// codenumberBase series

const (
	codenumberBase = 10000

	codenumberClientError = codenumberBase * iota
	codenumberServerError
)

// codenumberClientError series

const (
	CodeInvalidJSON = iota + codenumberClientError + 1
	CodeInvalidPayload
)

// codenumberServerError series

const (
	CodeUnavailableRedis = iota + codenumberServerError + 1
)

// functions

func getStatusCode(code int) int {
	inRange := makeRangeValidation(code)

	switch {
	case inRange(codenumberClientError):
		return http.StatusBadRequest
	case inRange(codenumberServerError):
		return http.StatusServiceUnavailable
	}
	return http.StatusInternalServerError
}

func makeRangeValidation(errorcode int) func(int) bool {
	return func(codebase int) bool {
		return codebase <= errorcode && errorcode < codebase+codenumberBase
	}
}
