module gitlab.com/tonypythoneer/shorturl-go

go 1.16

require (
	github.com/Azure/go-ansiterm v0.0.0-20210608223527-2377c96fe795 // indirect
	github.com/Microsoft/go-winio v0.5.0 // indirect
	github.com/andybalholm/brotli v1.0.3 // indirect
	github.com/cenkalti/backoff/v4 v4.1.1 // indirect
	github.com/containerd/continuity v0.1.0 // indirect
	github.com/go-playground/validator/v10 v10.6.1
	github.com/go-redis/redis/v8 v8.10.0 // indirect
	github.com/gofiber/fiber/v2 v2.12.0
	github.com/gofiber/storage/redis v0.0.0-20210602124502-f0233feaea00
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/klauspost/compress v1.13.0 // indirect
	github.com/konsorten/go-windows-terminal-sequences v1.0.3 // indirect
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/opencontainers/runc v1.0.0-rc95 // indirect
	github.com/ory/dockertest/v3 v3.6.5
	github.com/sirupsen/logrus v1.8.1 // indirect
	github.com/stretchr/testify v1.7.0
	github.com/valyala/fasthttp v1.26.0
	golang.org/x/net v0.0.0-20210525063256-abc453219eb5 // indirect
	golang.org/x/sys v0.0.0-20210608053332-aa57babbf139 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
