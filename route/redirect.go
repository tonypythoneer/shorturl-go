package route

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/tonypythoneer/shorturl-go/service/shorturl"
)

const (
	urlIDparm = "urlID"
)

func RegisterRedirect(parentGroup fiber.Router) fiber.Router {
	return parentGroup.Get(":"+urlIDparm, redirect)
}

func redirect(c *fiber.Ctx) error {
	urlID := c.Params(urlIDparm, "")
	url, err := shorturl.GetURL(urlID)
	if err != nil {
		return err
	} else if url == nil {
		return c.SendStatus(fiber.StatusNotFound)
	}

	return c.Redirect(string(url))
}
