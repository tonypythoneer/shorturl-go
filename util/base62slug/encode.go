package base62slug

import (
	"math/rand"
	"time"
)

const (
	alphabet       = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	alphabetLength = len(alphabet)
)

func Encode(setters ...SetOption) []byte {
	options := applyOptions(setters...)

	bytes := make([]byte, options.Length)
	newRand := rand.New(
		rand.NewSource(
			time.Now().UnixNano() + options.Seed,
		),
	)

	for i := 0; i < options.Length; i++ {
		number := newRand.Intn(alphabetLength)
		bytes[i] = alphabet[number]
	}

	return bytes
}
