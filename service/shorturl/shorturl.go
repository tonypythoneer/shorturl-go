package shorturl

import (
	"time"

	"gitlab.com/tonypythoneer/shorturl-go/app/redis"
	"gitlab.com/tonypythoneer/shorturl-go/util/errorcode"
)

// reduce repeatable error wrapping &
// avoid dispersing hte logic of accessing the same instance in every file
// provide unified interface to let controller import

// it can improve development experience
// lower the mental burden when reading lengthy context in a single function

// create

type CreateURLOption struct {
	HashedKey     string
	SlugKey       string
	URL           string
	ExpiredPeriod time.Duration
}

func CreateURL(opt CreateURLOption) error {
	set := redis.SimpleRedisStorage.Set
	if err := set(opt.HashedKey, []byte(opt.SlugKey), opt.ExpiredPeriod); err != nil {
		return errorcode.NewError(errorcode.CodeUnavailableRedis, err.Error())
	} else if err := set(opt.SlugKey, []byte(opt.URL), opt.ExpiredPeriod); err != nil {
		return errorcode.NewError(errorcode.CodeUnavailableRedis, err.Error())
	}
	return nil
}

// read

func GetURL(key string) ([]byte, error) {
	url, err := redis.SimpleRedisStorage.Get(key)
	if err != nil {
		return nil, errorcode.NewError(errorcode.CodeUnavailableRedis, err.Error())
	}
	return url, nil
}

// update
// no

// delete
func DeleteURL(key string) error {
	if err := redis.SimpleRedisStorage.Delete(key); err != nil {
		return errorcode.NewError(errorcode.CodeUnavailableRedis, err.Error())
	}
	return nil
}
