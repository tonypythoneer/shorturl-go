package errorcode

import (
	"encoding/json"
	"fmt"
)

type Error struct {
	Code    int         `json:"code"`
	Details interface{} `json:"details,omitempty"`
}

func (e *Error) Error() string {
	details, _ := json.Marshal(e.Details)
	return fmt.Sprintf("%d: %s", e.Code, string(details))
}

func (e *Error) GetStatusCode() int {
	return getStatusCode(e.Code)
}

func NewError(code int, details ...interface{}) *Error {
	e := &Error{Code: code}
	if len(details) > 0 {
		e.Details = details[0]
	}
	return e
}
