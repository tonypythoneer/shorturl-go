# run infra
.PHONY: up
up:
	docker-compose up

# build/update vendor
.PHONY: mod
mod:
	go mod tidy
	go mod vendor

# run service
.PHONY: run
run:
	go run main.go

# run testing
.PHONY: test
test:
	go test ./... -v
